<?php

/**
 * @file
 * Class definition of RitzauWireProcessor.
 */

/**
 * Creates nodes from feed items.
 */
class RitzauProcessor extends FeedsNodeProcessor {
  protected function entityLoad(FeedsSource $source, $nid) {
    $node = parent::entityLoad($source, $nid);
    return $node;
  }
  public function entitySave($entity) {
    $collections = array();
    foreach (field_info_instances('node', $entity->type) as $name => $instance) {
      $info = field_info_field($name);
      if ($info['type'] == 'field_collection') {
      }

    }
    $keys = array_keys((array)$entity);
    foreach($keys as $value){
        $exp_key = explode(':', $value);
        if(count($exp_key) > 1){
             $arr_result[$exp_key[0]][] = $exp_key[1];
        }
    }
    foreach ($arr_result as $field_collection_name => $fields) {
      if(!isset($entity->{$field_collection_name}[$entity->language][0]['value'])) {
        
        ${$field_collection_name} = entity_create('field_collection_item', array('field_name' => $field_collection_name));
        ${$field_collection_name}->setHostEntity('node', $entity);
        foreach ($fields as $field_name) {
          ${$field_collection_name}->{$field_name} = $entity->{$field_collection_name . ':' . $field_name};
          unset($entity->{$field_collection_name . ':' . $field_name});
        }
        ${$field_collection_name}->save();
        $entity->{$field_collection_name}[$entity->language][0]['value'] = ${$field_collection_name}->item_id;
      }
    }
    parent::entitySave($entity);
  }
  protected function map(FeedsSource $source, FeedsParserResult $result, $target_item = NULL) {
    // Static cache $targets as getMappingTargets() may be an expensive method.
    static $sources;
    if (!isset($sources[$this->id])) {
      $sources[$this->id] = feeds_importer($this->id)->parser->getMappingSources();
    }
    static $targets;
    if (!isset($targets[$this->id])) {
      $targets[$this->id] = $this->getMappingTargets();
    }
    $parser = feeds_importer($this->id)->parser;
    if (empty($target_item)) {
      $target_item = array();
    }

    // Many mappers add to existing fields rather than replacing them. Hence we
    // need to clear target elements of each item before mapping in case we are
    // mapping on a prepopulated item such as an existing node.
    foreach ($this->config['mappings'] as $mapping) {
      if (isset($targets[$this->id][$mapping['target']]['real_target'])) {
        unset($target_item->{$targets[$this->id][$mapping['target']]['real_target']});
      }
      elseif (isset($target_item->{$mapping['target']})) {
        unset($target_item->{$mapping['target']});
      }
    }

    /*
     This is where the actual mapping happens: For every mapping we envoke
     the parser's getSourceElement() method to retrieve the value of the source
     element and pass it to the processor's setTargetElement() to stick it
     on the right place of the target item.
     
     If the mapping specifies a callback method, use the callback instead of
     setTargetElement().
     */
    self::loadMappers();
    foreach ($this->config['mappings'] as $mapping) {
      // Retrieve source element's value from parser.
      if (isset($sources[$this->id][$mapping['source']]) && 
            is_array($sources[$this->id][$mapping['source']]) && 
            isset($sources[$this->id][$mapping['source']]['callback']) && 
            function_exists($sources[$this->id][$mapping['source']]['callback'])) {
        $callback = $sources[$this->id][$mapping['source']]['callback'];
        $value = $callback($source, $result, $mapping['source']);
      }
      else {
        $value = $parser->getSourceElement($source, $result, $mapping['source']);
      }

      // Map the source element's value to the target.
      if (isset($targets[$this->id][$mapping['target']]) && 
            is_array($targets[$this->id][$mapping['target']]) && 
            isset($targets[$this->id][$mapping['target']]['callback']) && 
            function_exists($targets[$this->id][$mapping['target']]['callback'])) {
        $callback = $targets[$this->id][$mapping['target']]['callback'];
         
        if (($collection = strpos($mapping['target'], ':')) !== false ) {
          $target = substr($mapping['target'], $collection + 1);
          $target_tmp_item = (object) array();
          $callback($source, $target_tmp_item, $target, $value, $mapping);
          foreach ((array)$target_tmp_item as $key => $value) {
            $target_item->{substr($mapping['target'], 0, $collection) . ":" . $target} = $value;
          }
        }
        else {
          $callback($source, $target_item, $mapping['target'], $value, $mapping);
        }
      }
      else {
        $this->setTargetElement($source, $target_item, $mapping['target'], $value, $mapping);
      }
    }
    return $target_item;
  }
}