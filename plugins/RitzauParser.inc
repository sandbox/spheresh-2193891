<?php
/**
 * @file
 * This file contains the Ritzau feeds parser.
 */

class RitzauParser extends FeedsParser {

  /**
   * Implements FeedsParser::parse().
   */
  public function parse(FeedsSource $source, FeedsFetcherResult $fetcher_result) {

    $items = $fetcher_result->getRaw();
    /*$item = $items[0];*/
    foreach ($items as $key1 => $item) {
      $news=$fetcher_result->client->getNews($item->id);
      foreach ($news->pictures as $key2 => $picture) {
        $ritzauid=$picture->fullHiresFileName;
        //$slideshow->setImageCaption($ritzau_images[$ritzauid],$picture['caption']);
        if($ritzauid == 'null.jpg') {
          $news->pictures[$key2] = '';
          
        }
        else {
          $filename = $fetcher_result->client->getFile($ritzauid);
          $news->pictures[$key2] = $filename;
          
        }
      }
      $items[$key1] = (array)$news;
    }
    return new FeedsParserResult($items);
  }

  /**
   * Return mapping sources.
   *
   * At a future point, we could expose data type information here,
   * storage systems like Data module could use this information to store
   * parsed data automatically in fields with a correct field type.
   */
  public function getMappingSources() {
    $sources = array();
    $fieldRitzauNews = new RitzauNews();
    foreach ((array)$fieldRitzauNews as $key => $value) {
        $sources[$key] = array(
          'name' => t('RitzauNews : ' .ucwords($key)),
        );
     } 
    $sources['pictures'] = array(
      'name' => t('RitzauPicture : Pictures'),
    );
    return $sources;
    array(
      'pictures' => array(
      ),
      'headline' => array(
        'name' => t('Headline'),
        'description' => t('Title of the wire'),
      ),
      'body' => array(
        'name' => t('Body'),
        'description' => t('Body'),
      ),
      'ritzau_id' => array(
        'name' => t('Ritzau ID'),
        'description' => t('Unique ID for the imported content.'),
      ),
      'refering_ritzau_id' => array(
        'name' => t('Refering Ritzau ID'),
        'description' => t('Refering Unique ID for the imported content.'),
      ),
      'city' => array(
        'name' => t('City'),
        'description' => t('City'),
      ),
      'source' => array(
        'name' => t('Source'),
        'description' => t('Source'),
      ),
      'category' => array(
        'name' => t('Category'),
        'description' => t('Category'),
      ),
      'category_tid' => array(
        'name' => t('Category tid'),
        'description' => t('Category tid'),
      ),
      'genre' => array(
        'name' => t('Genre'),
        'description' => t('Genre'),
      ),
      'genre_tid' => array(
        'name' => t('Genre tid'),
        'description' => t('Genre tid'),
      ),
      'iptc_subjects_string' => array(
        'name' => t('IPTC Subjects string'),
        'description' => t('IPTC Subjects string'),
      ),
      'iptc_subjects_array' => array(
        'name' => t('IPTC Subjects array'),
        'description' => t('IPTC Subjects array'),
      ),
      'iptc_subjects_tids' => array(
        'name' => t('IPTC Subjects tids'),
        'description' => t('IPTC Subjects tids'),
      ),
      'caption' => array(
        'name' => t('Caption'),
        'description' => t('Caption'),
      ),
      'urgency' => array(
        'name' => t('Urgency'),
        'description' => t('Urgency'),
      ),
      'status' => array(
        'name' => t('Status'),
        'description' => t('Status'),
      ),
      'published' => array(
        'name' => t('Published'),
        'description' => t('Published'),
      ),
      'log' => array(
        'name' => t('Log'),
        'description' => t('Log message'),
      ),
      'reference' => array(
        'name' => t('Reference'),
        'description' => t('Reference'),
      ),
      'sent_date' => array(
        'name' => t('Sent date'),
        'description' => t('Sent date'),
      ),
      'sent_time' => array(
        'name' => t('Sent time'),
        'description' => t('Sent time'),
      ),
      'sent_datetime' => array(
        'name' => t('Sent date & time'),
        'description' => t('Sent date & time'),
      ),
      'update_datetime' => array(
        'name' => t('Update date & time'),
        'description' => t('Update date & time'),
      ),
      'release_date' => array(
        'name' => t('Release date'),
        'description' => t('Release date'),
      ),
      'release_time' => array(
        'name' => t('Release time'),
        'description' => t('Release time'),
      ),
      'release_datetime' => array(
        'name' => t('Release date & time'),
        'description' => t('Release date & time'),
      ),
      'xmlfile' => array(
        'name' => t('Ritzau xml file'),
        'description' => t('Ritzau xml file'),
      ),
    );// + parent::getMappingSources();
  }
  
}