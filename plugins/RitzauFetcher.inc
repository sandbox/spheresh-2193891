<?php
/**
 * @file
 * Home of the RitzauFetcher and related classes.
 */

/**
 * Definition of the import batch object created on the fetching stage by
 * FeedsSoapFetcher.
 */
class RitzauFetcherResult extends FeedsFetcherResult {

  protected $config;
  public $client;

  /**
   * Constructor.
   */
  public function __construct($config) {
    $this->config = $config;

    $classmap = array(
      "News"=>"RitzauNews",
      "NewsListItem"=>"RitzauNewsListItem",
      "Picture"=>"RitzauPicture",
    );
    $soap_options = array(
      "encoding"=>"UTF8",
      "exceptions"=>true,
      "connection_timeout"=>10,
      "classmap"=>$classmap,
    );
    $this->client = new Ritzau($this->config['endpoint'], $this->config['username'], $this->config['password']);

    parent::__construct();
  }

  /**
   * Implementation of FeedsImportBatch::getRaw().
   */
  public function getRaw() {
    try {

      $items = $this->client->getNewsListItems(time() - (3600 * 24)/*$this->config['date']*/, 
      	$this->config['dateModifier'], $this->config['category'], 
      	$this->config['language'], $this->config['iptc'], 
      	$this->config['topStory'], $this->config['limit'], 
      	$this->config['orderBy'], $this->config['fetchOnlyOnline']);
      return $items;
    } catch (SoapFault $fault) {
      drupal_set_message("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", 'error');
      return;
    }    
  }
}
/**
 * Fetches data via SOAP.
 */
class RitzauFetcher extends FeedsFetcher {

  /**
   * Implementation of FeedsFetcher::fetch().
   */
  public function fetch(FeedsSource $source) {
    // Note: we fetch data using the Feed config instead of using the source
    // import form. The reason for this, is that does not make sense allowing
    // the user to switch the SOAP endpoint, when it requires a much more
    // complex configuration.
    return new RitzauFetcherResult($this->config);
  }

  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    return array(
      'endpoint' => '',
      'username' => '',
      'password' => '',
      'date' => "",
      'dateModifier' => "after",
      'category' => "",
      'language' => "",
      'iptc' => "",
      'topStory' => "",
      'limit' => -1,
      'orderBy' => "pubdate",
      'fetchOnlyOnline' => true
    );
  }
  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {
    $form['endpoint'] = array(
      '#type' => 'textfield',
      '#title' => t('SOAP server endpoint URL'),
      '#size' => 60,
      '#maxlength' => 256,
      '#description' => t('Enter the absolute endpoint URL of the SOAP Server service. If WSDL is being used (see SOAPFetcher settings), this will be the URL to retrieve the WSDL.'),
      '#default_value' => $this->config['endpoint'],
      '#required' => TRUE
    );

    $form['username'] = array(
      '#type' => 'textfield',
      '#title' => 'Username',
      '#default_value' => $this->config['username'],
    );

    $form['password'] = array(
      '#type' => 'textfield',
      '#title' => 'Password',
      '#default_value' => $this->config['password'],
    );

    $args = array_diff(array_keys($this->configDefaults()), array('endpoint', 'username', 'password'));
    foreach ($args as $key) {
	    $form[$key] = array(
	      '#type' => 'textfield',
	      '#title' => $key,
	      '#default_value' => $this->config[$key],
	    );
    }
    return $form;
  }
  /**
   * Expose source form.
   */
  public function sourceForm($source_config) {
    $form = array();

    $args = array_keys($this->configDefaults());
    foreach ($args as $key) {
	    $form[$key] = array(
	      '#type' => 'textfield',
	      '#title' => $key,
	      '#default_value' => $this->config[$key],
	    );
    }
    return $form;
  }
}
