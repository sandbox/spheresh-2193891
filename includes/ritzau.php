<?php
/*
	Eksempel - oversigt:
	================================================
	$ritzau = new Ritzau();

	foreach($ritzau->getNewsListItems() as $item) {
		print $item->headline."\n";
	}

	Eksempel - vis telegram med id $news_id:
	================================================
	$ritzau = new Ritzau();

	$item = $ritzau->getNews($news_id);
	print $item->headline."\n";
	print $item->body."\n";

	if (is_array($item->sounds)) {
		foreach($item->sounds as $sound) {
			print "Lydklip: ".$sound->caption."\n";
		}
	}
*/

class RitzauNews {
	public $body;
	public $caption;
	public $category;
	public $date;
	public $dateSendt;
	public $foundDate;
	public $headline;
	public $id;
	public $iptcCategory;
	public $language;
	public $master;
	public $online;
	public $pictures;
	public $priority;
	public $priorityString;
	private $properties;
	public $references;
	public $sounds;
	public $source;
	public $teaser;
	public $telegramFilePath;
	public $telegramId;
	public $telegramPath;
	public $timeSendt;
	public $topStoryCategory;
	public $topStoryOveral;
	public $twinId;
	public $videos;
	public $websites;

	public function getProperty($key) {
		foreach($this->properties as $property)
			if($property->key==$key)
				return $property->value;
		return false;
	}

	public function getFoundDate($format="") {
		return Ritzau::ISO8601_decode($this->foundDate, $format);
	}

	public function getDate($format="") {
		return Ritzau::ISO8601_decode($this->date, $format);
	}
}

class RitzauNewsListItem {
	public $caption;
	public $category;
	public $date;
	public $headline;
	public $id;
	public $iptcCategory;
	public $language;
	public $online;
	public $priority;
	public $source;
	public $topStoryAll;
	public $topStoryCat;

	public function getDate($format="") {
		return Ritzau::ISO8601_decode($this->date, $format);
	}
}

class RitzauPicture {
	public $byLine;
	public $caption;
	public $fullHiresFileName;
	public $fullLowresFileName;
	public $fullThumbFileName;
	public $fullXlFileName;
	public $hiresFileName;
	public $hiresFileType;
	public $hiresHeight;
	public $hiresWidth;
	public $lowresFileName;
	public $lowresFileType;
	public $lowresHeight;
	public $lowresWidth;
	public $priority;
	public $providerId;
	public $providerName;
	public $thumbFileName;
	public $thumbFileType;
	public $thumbHeight;
	public $thumbWidth;
	public $xlFileName;
	public $xlFileType;
	public $xlHeight;
	public $xlWidth;
	public $tmpFileName;

	public function hasAccess() {
		return $this->hiresFileName!='';
	}

	public function getFile() {
		if(!$this->hasAccess())
			return false;
		$client=new Ritzau();

		if($this->xlFileName)
			return $client->getFile($this->fullXlFileName);
		else
			return $client->getFile($this->fullHiresFileName);
	}
}

class Ritzau {
	private static $wsdl;
	private static $username;
	private static $password;
	private $client;

	public function __construct($wsdl, $username, $password) {
		self::$wsdl = $wsdl;
		self::$username = $username;
		self::$password = $password;
		$classmap = array(
			"News"=>"RitzauNews",
			"NewsListItem"=>"RitzauNewsListItem",
			"Picture"=>"RitzauPicture",
		);
		$soap_options = array(
			"encoding"=>"UTF8",
			"exceptions"=>true,
			"connection_timeout"=>10,
			"classmap"=>$classmap,
		);
		$this->client=new SoapClient(self::$wsdl,$soap_options);
	}

	public function getNewsListItems($date = "", $dateModifier = "after", $category = "", $language = "", $iptc = "", $topStory = "", $limit = -1, $orderBy = "pubdate", $fetchOnlyOnline = true) {
		$date=gmdate('Y-m-d\TH:i:s\Z',$date);
		try {
			$items=$this->client->getNewsListItems($date, $dateModifier, $category, $language, $iptc, $topStory, $limit, $orderBy, $fetchOnlyOnline, self::$username, self::$password);
		} catch (SoapFault $e) {
			return false;
		}
		return $items;
	}

	public function getNews($id) {
		try {
			$news=$this->client->getNews($id, self::$username, self::$password);
		} catch (SoapFault $e) {
			trigger_error("getNews('$id') failed", E_USER_WARNING); 
			return false;
		}
		return $news;
	}

	public function getFile($filename) {
		try {
			$result=$this->client->getFileAsBase64($filename, self::$username, self::$password);
		} catch (SoapFault $e) {
			echo "getFile('$filename') failed\n"; 
			return false;
		}

		$result=str_replace("\n","",$result);
		if(!($contents=base64_decode($result,true))) {
			echo "getFile('$filename') failed (base64_decode)\n"; 
			return false;
		}

		// HACK: Fix up jpg from broken web service (missing last byte(s)):
		// if last bytes == 0xff 0xd9 then do nothing
		// if last byte == 0xff then append 0xd9
		// else append 0xff 0xd9
		if(preg_match('/\.jpg$/',$filename) && !preg_match('/\xff\xd9$/',$contents)) {
			if(preg_match('/\xff$/',$contents))
				$contents.="\xd9";
			else
				$contents.="\xff\xd9";
		}

		$temp = tempnam("/tmp", "ritzau_file");
		file_put_contents($temp, $contents);
		return $temp;
	}

	/*
	 * $in er tidsformatet i iso8601 format, og $format er et evt. argument til date(). Er
	 * $format ikke tilstede returneres tidspunktet som et unix timestamp.
	 */
	public static function ISO8601_decode($in, $format = "") {
		preg_match("/^([0-9]{4})-([0-9]{2})-([0-9]{2})T([0-9]{2}):([0-9]{2}):([0-9]{2})\.([0-9]+)Z/", $in, $regs);

		$day    = $regs[3];
		$month  = $regs[2];
		$year   = $regs[1];

		$hour   = $regs[4];
		$minute = $regs[5];
		$second = $regs[6];

		$zone   = $regs[7];

		$timestamp = gmmktime($hour, $minute, $second, $month, $day, $year);

		if ($format)
			return date($format, $timestamp);
		else
			return $timestamp;
	}
}
?>
